package com.datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class dataprovider {
	@DataProvider(name = "logindata")
	public String[][] getData() throws IOException {
		File f = new File("/home/pradeep/Documents/Test_Data/Testdata2.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook book = new XSSFWorkbook(fis);
		XSSFSheet Sheet = book.getSheetAt(0);
		int rows = Sheet.getPhysicalNumberOfRows();
		int colum = Sheet.getRow(0).getLastCellNum();
		String[][] data = new String[rows - 1][colum];
		for (int i = 0; i < rows - 1; i++) {
			for (int j = 0; j < colum; j++) {
				DataFormatter df = new DataFormatter();
				data[i][j] = df.formatCellValue(Sheet.getRow(i + 1).getCell(j));
			}
		}

		return data;
	}
}