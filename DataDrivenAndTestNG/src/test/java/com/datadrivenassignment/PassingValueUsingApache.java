package com.datadrivenassignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PassingValueUsingApache {
	public static void main(String[] args) throws IOException {

		WebDriver driver = new ChromeDriver();
		driver.get(" https://demowebshop.tricentis.com/register");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		File f = new File("/home/pradeep/Documents/Test_Data/Testdata3.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook book = new XSSFWorkbook(fis);
		XSSFSheet sheet = book.getSheetAt(0);
		int rows = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {

			String FirstName = sheet.getRow(i).getCell(0).getStringCellValue();
			String LastName = sheet.getRow(i).getCell(1).getStringCellValue();
			String Email = sheet.getRow(i).getCell(2).getStringCellValue();
			String Password = sheet.getRow(i).getCell(3).getStringCellValue();
			String confirmpassword = sheet.getRow(i).getCell(4).getStringCellValue();

			WebElement radio = driver.findElement(By.xpath("//input[@type='radio']"));
			radio.click();
			WebElement first = driver.findElement(By.name("FirstName"));
			first.sendKeys(FirstName);
			WebElement last = driver.findElement(By.name("LastName"));
			last.sendKeys(LastName);
			WebElement user = driver.findElement(By.name("Email"));
			user.sendKeys(Email);
			WebElement pass = driver.findElement(By.name("Password"));
			pass.sendKeys(Password);
			WebElement cpass = driver.findElement(By.name("ConfirmPassword"));
			cpass.sendKeys(confirmpassword);
			WebElement reg = driver.findElement(By.xpath(" //input[@value='Register']"));
			reg.click();
			driver.findElement(By.linkText("Register")).click();
		}
		driver.quit();
	}
}
