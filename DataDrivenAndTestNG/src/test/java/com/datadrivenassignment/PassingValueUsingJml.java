package com.datadrivenassignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PassingValueUsingJml {

	public static void main(String[] args) throws IOException, BiffException {
		WebDriver driver = new ChromeDriver();
		driver.get(" https://demowebshop.tricentis.com/register");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		File f = new File("/home/pradeep/Documents/Test_Data/Testdata4.xls");
		FileInputStream fis = new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fis);
		Sheet sh = book.getSheet("Sheet1");
		int rows = sh.getRows();
		int column = sh.getColumns();
		for (int i = 1; i < rows; i++) {
			String FirstName = sh.getCell(0, i).getContents();
			String LastName = sh.getCell(1, i).getContents();
			String Email = sh.getCell(2, i).getContents();
			String Password = sh.getCell(3, i).getContents();
			String confirmpassword = sh.getCell(4, i).getContents();

			WebElement radio = driver.findElement(By.xpath("//input[@type='radio']"));
			radio.click();
			WebElement first = driver.findElement(By.name("FirstName"));
			first.sendKeys(FirstName);
			WebElement last = driver.findElement(By.name("LastName"));
			last.sendKeys(LastName);
			WebElement user = driver.findElement(By.name("Email"));
			user.sendKeys(Email);
			WebElement pass = driver.findElement(By.name("Password"));
			pass.sendKeys(Password);
			WebElement cpass = driver.findElement(By.name("ConfirmPassword"));
			cpass.sendKeys(confirmpassword);
			WebElement reg = driver.findElement(By.xpath(" //input[@value='Register']"));
			reg.click();
			driver.findElement(By.linkText("Register")).click();

		}
		driver.quit();

	}
}
