package com.testngassignment;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ActitimeTest {
	public String baseUrl ="https://demo.actitime.com/login.do";
	public WebDriver driver;
  @Test
  public void pleaseIdentify() {
	  String expected = "Please identify yourself";
	  String actual =driver.findElement(By.xpath("//td[text()='Please identify yourself']")).getText();
	  Assert.assertEquals(actual, expected);
	  System.out.println(actual);
  }
  @Test
  public void loginIdentify() {
	  String expected1 = "Login";
	  String actual1 =driver.findElement(By.id("loginButton")).getText();
	  Assert.assertEquals(actual1, expected1);
	  System.out.println(actual1);	 
  }
  public void logoIdentify() {
	  String expected2 = "actiTIME";
	  String actual2 = driver.getTitle();
	  Assert.assertEquals(actual2, expected2);
	  System.out.println(actual2);	 
  }
  @BeforeTest
  public void beforeTest() {
	  driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	  driver.get(baseUrl);
  }

  @AfterTest
  public void afterTest() {
	 // driver.quit();
  }

}
